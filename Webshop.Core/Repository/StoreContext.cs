﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Webshop.Core.Models;

namespace Webshop.Core.Repository
{
	public class StoreContext : DbContext
	{
		public StoreContext() : base("DefaultConnection")
		{

		}
		public DbSet<OrderItem> OrderItems { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<Customer> Customers { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
		}
	}
}
