﻿class CartSvc {

    static $inject = ["$rootScope", "$http"];

    constructor(
		private $rootScope: angular.IRootScopeService,
		private $http: angular.IHttpService) {		
	}

	getItems() {
		return this.$http.get("/cart/getItems");
    }

	addItem(id: number, quantity: number) {
		var params: any = {
			id: id,
			quantity: quantity
		};
        return this.$http.post("/cart/addItem", params);
    }

	updateItem(id: number, quantity: number) {
		var params: any = {
			id: id,
			quantity: quantity
		};
		return this.$http.post("/cart/updateItem", params);
    }

	removeItem(id: number) {
		return this.$http.post("/cart/removeItem", { ID: id });
    }
	
	clear() {
		return this.$http.get("/cart/clear");
    }

	updateCart() {
		this.$http.get("/cart/getCounter")
			.success((cart: ICartUpdated) => {
				this.$rootScope.$broadcast("cartUpdated", cart);
			});
    }
	
    checkout(cust: ICustomer) {
        return this.$http.post("/cart/checkout", cust);
    }
    
}

angular.module('storeApp').service('CartSvc', CartSvc);
