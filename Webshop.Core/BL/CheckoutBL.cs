﻿using Webshop.Core.Models;
using Webshop.Core.Repository;
using Webshop.Core.VO;

namespace Webshop.Core.BL
{
	public class CheckoutBL
	{
		public static CheckoutVO Register(Customer customer, Cart cart)
		{
			using(var db = new StoreContext())
			{
				//Customer Registration
				db.Customers.Add(customer);

				//Order Registration
				var order = new Order
				{
					Customer = customer,
					Items = cart.Items,
					SubTotal = cart.SubTotal,
					TotalVAT = cart.TotalVAT,
					Total = cart.Total
				};
				db.Orders.Add(order);

				db.SaveChanges();
				
				return new CheckoutVO
				{
					Success = true,
					CustomerID = customer.ID,
					OrderID = order.ID
				};
			}
		}
	}
}
