﻿angular
	.module("storeApp", ["ui.router", "ui.bootstrap"])
	.config(["$stateProvider", "$urlRouterProvider", ($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider) => {
		$urlRouterProvider.otherwise("/");

		$stateProvider
			.state("catalog", {
				url: "/",
				templateUrl: "/App/Pages/Catalog.html"	
			})			
			.state("cart", {
				cache: false,
				url: "/cart",
				templateUrl: "/App/Pages/Cart.html"
			})
			.state("checkout", {
				cache: false,
				url: "/checkout",
				templateUrl: "/App/Pages/Checkout.html"
			})
			.state("thanks", {
				url: "/thanks",
				templateUrl: "/App/Pages/Thanks.html"
			})
			.state("about", {
				url: "/about",
				templateUrl: "/App/Pages/About.html"
			})
			;
	}]);