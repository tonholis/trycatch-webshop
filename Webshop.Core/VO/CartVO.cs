﻿using System.Collections.Generic;

namespace Webshop.Core.VO
{	
	public class CartVO
	{
		public int TotalCount { get; set; }
		public decimal TotalAmount { get; set; }
	}

}
