﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Webshop.Core.Models
{
	public class OrderItem
	{
		public int ID { get; set; }
		public string Code { get; set; }
		public string Title { get; set; }
		public Decimal Price { get; set; }
		public int Quantity { get; set; }
		public int OrderId { get; set; }
		public virtual Order Order { get; set; }
		
		/// <summary>
		/// Amount total of item
		/// </summary>
		public Decimal SubTotal
		{
			get
			{
				return Price * Quantity;
			}
		}
	}

	internal class OrderItemMap : EntityTypeConfiguration<OrderItem>
	{
		public OrderItemMap()
		{
			HasKey(x => x.ID);
		}
	}
}
