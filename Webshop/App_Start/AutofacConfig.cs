﻿using Autofac;
using Autofac.Features.ResolveAnything;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using Webshop.Core.Repository;

namespace Webshop.App_Start
{
	public static class AutofacConfig
	{
		public static void Setup()
		{
			var builder = new ContainerBuilder();
			var currentAssembly = Assembly.GetExecutingAssembly();

			builder.RegisterControllers(currentAssembly);

			builder.RegisterApiControllers(currentAssembly);

			builder.RegisterSource(new AnyConcreteTypeNotAlreadyRegisteredSource());

			builder.Register(ctx => ProductRepository.Create(HostingEnvironment.MapPath("~/App_Data/catalog.xml"))).SingleInstance();

			builder.RegisterType<StoreContext>().InstancePerRequest();

			var container = builder.Build();

			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

			var config = GlobalConfiguration.Configuration;
			config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
		}
	}
}