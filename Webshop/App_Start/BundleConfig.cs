﻿using System.Web;
using System.Web.Optimization;

namespace Webshop
{
	public class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/core").Include(
						"~/Scripts/jquery-{version}.js",						
						"~/Scripts/bootstrap.js",
						"~/Scripts/angular.min.js",
						"~/Scripts/angular-ui-router.min.js",
						"~/Scripts/angular-ui/ui-bootstrap-tpls.js"
					  ));

			bundles.Add(new ScriptBundle("~/bundles/app")
				.IncludeDirectory("~/App", "*.js", true));

			bundles.Add(new StyleBundle("~/Content/css").Include(
					  "~/Content/bootstrap.css",
					  "~/Content/site.css"));
		}
	}
}
