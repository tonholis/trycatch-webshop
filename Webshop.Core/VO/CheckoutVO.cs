﻿using System.Collections.Generic;

namespace Webshop.Core.VO
{
	public class CheckoutVO
	{
		public bool Success { get; set; }
		public int CustomerID { get; set; }
		public int OrderID { get; set; }

		public IDictionary<string, string[]> Errors { get; set; }
	}

}
