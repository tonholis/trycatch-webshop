﻿class CartCtrl {
	static $inject = ["CartSvc", "$rootScope"];

    constructor(
		private CartSvc: CartSvc,
		private $rootScope: angular.IRootScopeService)
	{
        this.getItems();
    }

	data: ICartResult;

	refresh(cart: ICartResult) {
		this.$rootScope.$broadcast("cartUpdated", cart);
		this.getItems();
	}
	
    getItems() {
        this.CartSvc.getItems()
            .success((result: ICartResult) => {
                this.data = result;
            });
    }

	clear() {
        this.CartSvc.clear()
			.success((cart: ICartResult) => {
				this.$rootScope.$broadcast("cartUpdated", cart);
				this.getItems();
			});
    }

	removeItem(p: IProduct) {
		this.CartSvc.removeItem(p.ID)
			.success((cart: ICartResult) => {
				this.$rootScope.$broadcast("cartUpdated", cart);
				this.getItems();
			});
	}

	updateItem(p: IProduct) {
		this.CartSvc.updateItem(p.ID, p.Quantity)
			.success((cart: ICartResult) => {
				this.$rootScope.$broadcast("cartUpdated", cart);
				this.getItems();
			});
	}
}

angular
	.module("storeApp")
	.controller("CartCtrl", CartCtrl);