﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Webshop.Core.Models
{
	public class Customer
	{
		public int ID { get; set; }
		public string Title { get; set; }

		[Required(ErrorMessage = "Please give us your first name")]
		public string FirstName { get; set; }

		[Required(ErrorMessage = "Please give us your last name")]
		public string LastName { get; set; }

		[Required(ErrorMessage = "Please give us your email address")]
		[EmailAddress(ErrorMessage = "Please give us a valid email address")]
		public string Email { get; set; }

		[Required(ErrorMessage = "Please give us your address")]
		public string Address { get; set; }

		[Required(ErrorMessage = "Please give us your address number")]
		public string AddressNumber { get; set; }

		[Required(ErrorMessage = "Please give us your zip code")]
		public string ZipCode { get; set; }

		[Required(ErrorMessage = "Please give us your city")]
		public string City { get; set; }		
	}

}
