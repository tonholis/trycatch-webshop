﻿using System;
using System.Collections.Generic;
using System.Linq;
using Webshop.Core.VO;

namespace Webshop.Core.Models
{
	public class Cart
	{
		public Cart()
		{
			VAT = 0.10M; //default tax
			Items = new List<OrderItem>();
		}
		
		public Decimal VAT { get; set; }

		public List<OrderItem> Items { get; private set; }

		public void AddItem(Product item, int quantity)
		{
			var cartItem = Items.Where(x => x.ID == item.ID).FirstOrDefault();
			if (cartItem == null)
			{
				cartItem = new OrderItem() {
					ID = item.ID,
					Code = item.Code,
					Title = item.Title,
					Price = item.Price,
					Quantity = quantity					
				};
				Items.Add(cartItem);
			}
			else
			{
				cartItem.Quantity += quantity;
			}			
		}

		public void UpdateItem(int id, int quantity)
		{
			var cartItem = Items.Where(x => x.ID == id).FirstOrDefault();
			if (cartItem != null)
				cartItem.Quantity = quantity;
		}

		public void RemoveItem(int id)
		{
			var cartItem = Items.Where(x => x.ID == id).FirstOrDefault();			
			if (cartItem != null)
				Items.Remove(cartItem);
		}

		public Decimal SubTotal
		{
			get
			{
				return Items.Sum(x => x.SubTotal);
			}
		}

		public Decimal Total
		{
			get
			{
				return SubTotal + TotalVAT;
			}
		}

		public Decimal TotalVAT
		{
			get
			{
				return SubTotal * VAT;
			}
		}
	}

}
