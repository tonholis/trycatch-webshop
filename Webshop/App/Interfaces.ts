﻿interface IProduct {
	ID: number;
	Code: string;
	Title: string;
	Price: number;
	Description: string;
	Quantity: number;
}

interface IProductDetail {
	Product: IProduct;
	addCart();
}

interface IPagingResult {
    Records?: any[];
    TotalCount?: number;
    CurrentPage?: number;
}

interface ISearchParams {
    Page: number;
    PageSize: number;
}

interface ICartUpdated {
    TotalCount: number;
    TotalAmount: number;
}

interface ICartResult {
    Items: IProduct[];
	SubTotal?: number;
	TotaVAT?: number;
	Total?: number;
}

interface ICustomer {
	Title: string;
	FirstName: string;
	LastName: string;
	Email: string;
	Address: string;
	AddressNumber: string;
	ZipCode: string;
	City: string;
}

interface ICheckoutResult {
	Success: boolean;
	OrderID: number;
	CustomerID: number;
	Errors: any[];
}