﻿using System.Collections.Generic;
using System.Linq;

namespace Webshop.Models
{
	public class PagingResult<T>
	{
		public PagingResult(IEnumerable<T> pagedRecords, int totalCount, int currentPage)
		{
			Records = pagedRecords;
			TotalCount = totalCount;
			CurrentPage = currentPage;
		}

		public IEnumerable<T> Records { get; private set; }
		public int TotalCount { get; private set; }
		public int CurrentPage { get; private set; }
	}

	public static class PagingResult
	{
		public static PagingResult<T> Create<T>(IEnumerable<T> allRecords, int pageSize, int currentPage)
		{
			var totalCount = allRecords.Count();
			var start = (currentPage - 1) * pageSize;
			var pageRecords = allRecords.Skip(start).Take(pageSize);

			return new PagingResult<T>(pageRecords, totalCount, currentPage);
		}
	}
}
