﻿class CatalogSvc {

    static $inject = ["$http"];

    constructor(private $http: angular.IHttpService) {
		
	}

    search(params: ISearchParams) {
        return this.$http.get("/api/catalog", { "params": params });
    }

    detail(itemId) {
        return this.$http.get("/api/catalog/" + itemId);
    }
}

angular.module('storeApp').service('CatalogSvc', CatalogSvc);
