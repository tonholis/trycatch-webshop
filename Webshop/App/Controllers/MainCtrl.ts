﻿class MainCtrl {
	static $inject = ["CartSvc", "$rootScope"];

    constructor(
		private CartSvc: CartSvc,
		private $rootScope: angular.IRootScopeService)
	{
		this.$rootScope["cartCount"] = "(empty)";
		this.$rootScope["cartTotal"] = 0;

        this.$rootScope.$on("cartUpdated", this.cartUpdated);
		this.CartSvc.updateCart();
    }

    cartUpdated(e: angular.IAngularEvent, cart: ICartUpdated) {
		var desc = "(empty)";

		if (cart.TotalCount > 0)
			desc = `(${cart.TotalCount})`;
		
		e.currentScope["cartCount"] = desc;
		e.currentScope["cartTotal"] = cart.TotalAmount;
    }	
}

angular
	.module("storeApp")
	.controller("MainCtrl", MainCtrl);