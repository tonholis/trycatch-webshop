﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webshop.Core.Models
{
	public class Order
	{
		public int ID { get; set; }
		public Decimal SubTotal { get; set; }
		public Decimal TotalVAT { get; set; }
		public Decimal Total { get; set; }
		
		public virtual List<OrderItem> Items { get; set; }

		public int CustomerID { get; set; }
		public virtual Customer Customer { get; set; }
	}

	internal class OrderMap : EntityTypeConfiguration<Order>
	{
		public OrderMap()
		{
			HasKey(b => b.ID);			
		}
	}
}
