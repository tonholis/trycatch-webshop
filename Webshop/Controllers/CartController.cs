﻿using System.Web.Mvc;
using Webshop.Core.Models;
using Webshop.Core.VO;
using Webshop.Core.BL;
using System.Linq;
using Webshop.Core.Repository;

namespace Webshop.Controllers
{
	public class CartController : Controller
	{
		private ProductRepository _productRepo;

		public CartController(ProductRepository repo)
		{
			_productRepo = repo;
		}

		public Cart Cart {
			get
			{
				if (HttpContext.Session["Cart"] == null)
					HttpContext.Session["Cart"] = new Cart();

				return (Cart)HttpContext.Session["Cart"];
			}
		}

		private CartVO GetCartState()
		{
			return new CartVO
			{
				TotalAmount = Cart.Total,
				TotalCount = Cart.Items.Sum(x => x.Quantity)
			};
		}

		[HttpPost]
		public ActionResult Clear()
		{			
			HttpContext.Session["Cart"] = new Cart();
			return Json(GetCartState());

		}

		[HttpPost]
		public ActionResult Checkout(Customer cust)
		{
			if (ModelState.IsValid)
			{
				var result = CheckoutBL.Register(cust, Cart);

				//Clear the cart
				HttpContext.Session["Cart"] = new Cart();

				return Json(result);
			}

			return Json(new CheckoutVO {
				Success = false,
				Errors = ModelState.ToDictionary(
					p => p.Key, 
					p => p.Value.Errors.Select(e => e.ErrorMessage).ToArray())				
			});
		}

		[HttpGet]
		public ActionResult GetCounter()
		{
			return Json(GetCartState(), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult AddItem(int id, int quantity)
		{
			Product item = _productRepo.Products.Where(x => x.ID == id).FirstOrDefault();
			Cart.AddItem(item, quantity);

			return Json(GetCartState());
		}

		[HttpPost]
		public ActionResult UpdateItem(int id, int quantity)
		{
			Cart.UpdateItem(id, quantity);

			return Json(GetCartState());
		}

		[HttpPost]
		public ActionResult RemoveItem(int id)
		{
			Cart.RemoveItem(id);

			return Json(GetCartState());
		}

		[HttpGet]
		public ActionResult GetItems()
		{
			return Json(Cart, JsonRequestBehavior.AllowGet);
		}

	}
}