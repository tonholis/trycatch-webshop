﻿class CatalogCtrl {
	static $inject = ["CatalogSvc", "CartSvc", "$uibModal"];

    constructor(
		private CatalogSvc: CatalogSvc,
		private CartSvc: CartSvc,		
		private $uibModal: angular.ui.bootstrap.IModalService)
	{
        this.search();
		//CartSvc.updateCart();
    }

	data: IPagingResult;

    params: ISearchParams = {
        Page: 1,
		PageSize: 10
    }

    search() {
        this.CatalogSvc.search(this.params)
            .success((result: IPagingResult) => {
                this.data = result;
                $(window).scrollTop(0);
            });
    }

	showDetails(item: IProduct) {
		this.$uibModal.open({
            templateUrl: 'modalDetail',
            controller: 'ProductDetailCtrl as vm',
            resolve: {
                product: item
            }
        });
	}

}

class ProductDetailCtrl {

    static $inject = ["$rootScope", "$uibModalInstance", "CartSvc", "product"];

    constructor(
		private $rootScope: angular.IRootScopeService,
		private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
		private CartSvc: CartSvc,
		private product: IProduct) {

		if (!this.product.Quantity)
			this.product.Quantity = 1;
    }

    addCart() {
		this.CartSvc.addItem(this.product.ID, this.product.Quantity)
            .success((cart: ICartUpdated) => {
				this.$rootScope.$broadcast("cartUpdated", cart);
				this.$uibModalInstance.close();
            });
    }
	
}

angular
	.module("storeApp")
	.controller("CatalogCtrl", CatalogCtrl)
	.controller("ProductDetailCtrl", ProductDetailCtrl);