﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Webshop.Core.Models
{
	public class Product
	{
		public int ID { get; set; }

		public string Code { get; set; }

		public string Title { get; set; }

		public Decimal Price { get; set; }

		public string Description { get; set; }
		
	}
}
