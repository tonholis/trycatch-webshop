﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Webshop.Core.Models;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;

namespace Webshop.Core.Repository
{
	public class ProductRepository
	{
		public IReadOnlyCollection<Product> Products { get; set; }

		public static ProductRepository Create(string path)
		{
			if (!File.Exists(path))
				throw new FileNotFoundException();
			
			XElement doc = XElement.Load(path);
			var items = from p in doc.Descendants("product")
							select new Product {
								ID = Convert.ToInt32(p.Attribute("id").Value),
								Code = p.Element("code").Value,
								Title = p.Element("title").Value,
								Description = p.Element("description").Value,
								Price = Convert.ToDecimal(p.Element("price").Value)
							};

			Debug.WriteLine("Products loaded from XML");
						
			return new ProductRepository
			{
				Products = items.ToList()
			};
		}
	}	
}
