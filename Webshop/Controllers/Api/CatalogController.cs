﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Webshop.Core.Models;
using Webshop.Core.Repository;
using Webshop.Models;

namespace Webshop.Controllers.Api
{
	public class CatalogController : ApiController
	{
		private ProductRepository _repo;

		public CatalogController(ProductRepository repo)
		{
			_repo = repo;
		}

		//GET /catalog?page=
		public object Get(int page = 1, int pageSize = 10)
		{
			return PagingResult.Create<Product>(_repo.Products, pageSize, page);
		}
	}
}
