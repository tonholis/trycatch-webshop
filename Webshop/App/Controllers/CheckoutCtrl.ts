﻿class CheckoutCartCtrl {
	static $inject = ["CartSvc", "$state"];

    constructor(
		private CartSvc: CartSvc,
		private $state: angular.ui.IStateService)
	{
	}  

	cust: ICustomer;
	result: ICheckoutResult;
	processing: boolean = false;

	proceed() { 
		this.processing = true;
		this.CartSvc.checkout(this.cust)
            .success((result: ICheckoutResult) => {
				this.result = result;

				if (result.Success) {
					this.CartSvc.updateCart();
					this.$state.go("thanks");
				}				

				this.processing = false;
            });
	}
}

angular
	.module("storeApp")
	.controller("CheckoutCartCtrl", CheckoutCartCtrl);